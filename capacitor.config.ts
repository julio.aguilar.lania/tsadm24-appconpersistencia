import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'rysi2024.appconpersistencia',
  appName: 'AppConPersistencia',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
