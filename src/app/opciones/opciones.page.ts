import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RangeCustomEvent } from '@ionic/angular';
import { IonContent, IonHeader, IonTitle, IonToolbar, IonGrid, IonRow, IonCol, IonSelect, IonSelectOption, IonRange, IonButton} from '@ionic/angular/standalone';
import { Preferences } from '@capacitor/preferences';

@Component({
  selector: 'app-opciones',
  templateUrl: './opciones.page.html',
  styleUrls: ['./opciones.page.scss'],
  standalone: true,
  imports: [IonContent, IonHeader, IonTitle, IonToolbar, CommonModule, FormsModule, IonGrid, IonRow, IonCol, IonSelect, IonSelectOption, IonRange, IonButton]
})
export class OpcionesPage implements OnInit {
  LLAVE_PREFERENCIAS = "OPTIONS_KEY";

  nivel = "MEDIO";
  limites = {lower:0, upper:100}
  constructor() { }

  ngOnInit() {
  }

  guardarOpciones() {
    Preferences.set({
      key: this.LLAVE_PREFERENCIAS, 
      value: JSON.stringify({
        nivel: this.nivel, 
        limiteInferior: this.limites.lower,
        limiteSuperior: this.limites.upper})
      })
  }

  onLimitesModificados(ev:Event) {
    let valor = (ev as RangeCustomEvent).detail.value;
    console.log(valor);
    //this.limites.lower = valor;
  }

  leerOpciones() {
    Preferences.get({key:this.LLAVE_PREFERENCIAS})
      .then(res => {
        if (res.value) {
          let opciones = JSON.parse(res.value);
          console.log(opciones);
          this.nivel = opciones["nivel"];
          this.limites.lower = opciones["limiteInferior"];
          this.limites.upper = opciones["limiteSuperior"];
        }
      })
  }

  ionViewWillEnter() {
    this.leerOpciones();
  }

}
