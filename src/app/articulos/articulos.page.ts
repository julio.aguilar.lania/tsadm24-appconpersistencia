import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonContent, IonHeader, IonTitle, IonToolbar, IonList, IonItem } from '@ionic/angular/standalone';
import { Articulo } from '../bd/articulo.model';
import { ArticulosService } from '../bd/articulos.service';

@Component({
  selector: 'app-articulos',
  templateUrl: './articulos.page.html',
  styleUrls: ['./articulos.page.scss'],
  standalone: true,
  imports: [IonContent, IonHeader, IonTitle, IonToolbar, CommonModule, FormsModule, IonList, IonItem]
})
export class ArticulosPage implements OnInit {

  listaArticulos: Articulo[] = [];

  constructor(
    private artsService: ArticulosService
  ) { }

  ngOnInit() {
    this.artsService.recuperarArticulos()
      .subscribe(arts => this.listaArticulos = arts);
  }

}
