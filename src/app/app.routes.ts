import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'folder/inbox',
    pathMatch: 'full',
  },
  {
    path: 'folder/:id',
    loadComponent: () =>
      import('./folder/folder.page').then((m) => m.FolderPage),
  },
  {
    path: 'preferencias',
    loadComponent: () => import('./preferencias/preferencias.page').then( m => m.PreferenciasPage)
  },
  {
    path: 'opciones',
    loadComponent: () => import('./opciones/opciones.page').then( m => m.OpcionesPage)
  },
  {
    path: 'articulos',
    loadComponent: () => import('./articulos/articulos.page').then( m => m.ArticulosPage)
  },
  {
    path: 'nuevo-articulo',
    loadComponent: () => import('./nuevo-articulo/nuevo-articulo.page').then( m => m.NuevoArticuloPage)
  },
];
