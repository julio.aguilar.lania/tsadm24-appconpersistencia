import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonContent, IonHeader, IonTitle, IonToolbar, IonGrid, IonRow, IonCol, IonButton } from '@ionic/angular/standalone';
import { Preferences, SetOptions } from '@capacitor/preferences';

@Component({
  selector: 'app-preferencias',
  templateUrl: './preferencias.page.html',
  styleUrls: ['./preferencias.page.scss'],
  standalone: true,
  imports: [IonContent, IonHeader, IonTitle, IonToolbar, CommonModule, FormsModule, IonGrid, IonRow,IonCol, IonButton]
})
export class PreferenciasPage implements OnInit {

  listaPreferencias: SetOptions[] = [];
  preferencia: SetOptions = {key:'', value:''};
  constructor() { }

  ngOnInit() {
    console.log('Preferencias');
  }

  ionViewWillEnter() {
    this.cargarPreferencias();
  }

  cargarPreferencias() {
    Preferences.keys()
      .then(keyResult => {
        console.log('Llaves encontradas ', keyResult.keys.length);
        this.listaPreferencias = [];
        keyResult.keys.forEach(llave => {
          Preferences.get({key: llave})
            .then(val => {
              if (val.value)
              this.listaPreferencias.push({key: llave, value: val.value})
          })
        });
      });
  }

  guardarLlaveValor() {
    console.log('Guardando llave,valor');
    if (!this.preferencia?.key || !this.preferencia.value) {
      return;
    }
    Preferences.set({key: this.preferencia?.key, value: this.preferencia?.value})
      .then(() => {
        console.log('Se guardo', this.preferencia?.key);
        this.preferencia.key = '';
        this.preferencia.value = '';
        this.cargarPreferencias();
      });
  }

}
