import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonContent, IonHeader, IonTitle, IonToolbar, IonGrid, IonRow, IonCol, IonLabel,IonInput, IonButton, IonDatetime, IonDatetimeButton, IonModal } from '@ionic/angular/standalone';
import { Articulo } from '../bd/articulo.model';
import { ArticulosService } from '../bd/articulos.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nuevo-articulo',
  templateUrl: './nuevo-articulo.page.html',
  styleUrls: ['./nuevo-articulo.page.scss'],
  standalone: true,
  imports: [IonContent, IonHeader, IonTitle, IonToolbar, CommonModule, FormsModule, IonGrid, IonRow, IonCol, IonLabel,IonInput, IonButton,IonDatetime, IonDatetimeButton, IonModal]
})
export class NuevoArticuloPage implements OnInit {

  articulo: Articulo = {
    idArticulo: -1,
    nombre: '',
    descripcion: '',
    fechaRegistro: '',
    activo: false,
    conteo: 0
  }
  constructor(
    private artsService: ArticulosService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  guardar() {
    this.artsService.agregarArticulo(this.articulo)
      .then(res => {
        this.router.navigateByUrl("articulos");
      })
      .catch(err => { console.log('Error', err);})
  }

}
