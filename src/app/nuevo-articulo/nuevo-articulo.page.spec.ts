import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NuevoArticuloPage } from './nuevo-articulo.page';

describe('NuevoArticuloPage', () => {
  let component: NuevoArticuloPage;
  let fixture: ComponentFixture<NuevoArticuloPage>;

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoArticuloPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
