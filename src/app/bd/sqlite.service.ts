import { Injectable } from "@angular/core";
import { CapacitorSQLite, CapacitorSQLitePlugin, SQLiteConnection, SQLiteDBConnection } from "@capacitor-community/sqlite";

@Injectable()
export class SQLiteService {
    sqliteConnection!: SQLiteConnection;
    sqlitePlugin!: CapacitorSQLitePlugin;

    async inicializarPlugin() : Promise<boolean> {
        console.log("inicializarPlugin");
        this.sqlitePlugin = CapacitorSQLite;
        this.sqliteConnection = new SQLiteConnection(this.sqlitePlugin);
        return true;
    }

    async abrirConexion(
        nombreBD: string,
        encriptada: boolean,
        modo: string,
        version: number,
        soloLectura: boolean
    ) {
        let consistente = (await this.sqliteConnection.checkConnectionsConsistency()).result;
        let conectados = (await this.sqliteConnection.isConnection(nombreBD, soloLectura)).result;
        let bd : SQLiteDBConnection;
        if (consistente && conectados) {
            bd = await this.sqliteConnection.retrieveConnection(nombreBD, soloLectura);
        } else {
            bd = await this.sqliteConnection.createConnection(nombreBD, encriptada, modo, version, soloLectura);
        }
        await bd.open();
        bd.getVersion()
            .then(val => console.log('Conectados a SQLite', val.version));
        return bd;
    }

    async closeConnection(database:string, readonly?: boolean): Promise<void> {
        const readOnly = readonly ? readonly : false;
        return await this.sqliteConnection.closeConnection(database, readOnly);
    }

    async saveToStore(database:string) : Promise<void> {
        return await this.sqliteConnection.saveToStore(database);
    }
}