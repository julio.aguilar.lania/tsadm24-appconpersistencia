import { Injectable } from "@angular/core";
import { SQLiteService } from "./sqlite.service";
import { SQLiteDBConnection } from "@capacitor-community/sqlite";
import { environment } from "src/environments/environment";
import { InicializadorService } from "./inicializador.service";
import { BehaviorSubject, max } from "rxjs";
import { Articulo } from "./articulo.model";

@Injectable()
export class ArticulosService {
    private conexion!: SQLiteDBConnection;

    articulos: BehaviorSubject<Articulo[]> = new BehaviorSubject<Articulo[]>([]);

    constructor(
        private sqliteService: SQLiteService,
        private inicializadorService: InicializadorService
    ) { }

    recuperarArticulos() {
        console.log('Recuperando articulos')
        this.cargarArticulos();
        return this.articulos.asObservable();
    }

    cargarArticulos() {
        console.log('cargando articulos')
        this.inicializadorService.conexionBD.query("SELECT * FROM articulos")
            .then(resultado => {
                let arts = resultado.values as Articulo[]
                this.articulos.next(arts);
            });
    }

    async agregarArticulo(art: Articulo) {
        let maxId = 1;
        await this.inicializadorService.conexionBD.query("SELECT MAX(idArticulo) FROM articulos;").then(
            res => {
                if(res.values) {
                    console.log('Buscando max id', res.values[0]['MAX(idArticulo)']);
                    let resultado = res.values[0]['MAX(idArticulo)'] as number;
                    try { maxId = resultado + 1}
                    catch(err) { maxId = 1}
                    if (!maxId) maxId = 1;
                }
            }
        )
        //await this.inicializadorService.conexionBD.query("SELECT COUNT(*) FROM articulos;").then()
        await this.inicializadorService.conexionBD.run("INSERT INTO articulos VALUES(?,?,?,?,?,?)", 
            [maxId,art.nombre, art.descripcion, art.fechaRegistro, 
                art.activo?1:0, art.conteo]
        ).then(res => {
            console.log("Nuevos articulos: ", res.changes);
            this.cargarArticulos();
        })
    }

    
}