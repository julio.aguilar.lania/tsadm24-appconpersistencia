import { Injectable } from "@angular/core";
import { SQLiteService } from "./sqlite.service";
import { environment } from "src/environments/environment";
import { SQLiteDBConnection } from "@capacitor-community/sqlite";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class InicializadorService {

    conexionBD!: SQLiteDBConnection;
    constructor(
        private sqliteService: SQLiteService,
        private http: HttpClient
    ) {}

    async inicializarApp() {
        await this.sqliteService.inicializarPlugin().then(
            async (ret) => {
                this.inicializarBD(environment.NOMBRE_BD);
            }
        );
    }

    async inicializarBD(nombreBD: string) {
        console.log("inicializando " + nombreBD);
        this.sqliteService.abrirConexion(environment.NOMBRE_BD, false, 'no-encrypted', 1, false)
            .then(conexion => {
                console.log('Conexion abierta');
                this.conexionBD = conexion;
                conexion.isTable('articulos').then(existe => {
                    if (!existe.result) {
                        console.log('Creando tabla ARTICULOS');
                        conexion.execute("CREATE TABLE articulos(idArticulo INTEGER PRIMARY KEY, nombre TEXT, descripcion TEXT, fechaRegistro TEXT, activo INTEGER, conteo INTEGER)")
                            .then(res => {
                                this.http.get("/assets/articulos.sql",{responseType: "text"})
                                    .subscribe(res => {
                                        res.split(/\r?\n/).forEach(l => conexion.execute(l).then(res => {}))
                                    });
                            });
                    }
                })
            })
            .catch(err => {console.log("NO se pudo abrir conexion.", err)})
    }
}