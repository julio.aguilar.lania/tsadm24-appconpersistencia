export interface Articulo {
    idArticulo: number
    nombre: string
    descripcion: string
    fechaRegistro: string
    activo: boolean
    conteo: number
}