import { APP_INITIALIZER, enableProdMode } from '@angular/core';
import { bootstrapApplication } from '@angular/platform-browser';
import { RouteReuseStrategy, provideRouter } from '@angular/router';
import { IonicRouteStrategy, provideIonicAngular } from '@ionic/angular/standalone';

import { routes } from './app/app.routes';
import { AppComponent } from './app/app.component';
import { environment } from './environments/environment';
import { InicializadorService } from './app/bd/inicializador.service';
import { SQLiteService } from './app/bd/sqlite.service';
import { ArticulosService } from './app/bd/articulos.service';
import { provideHttpClient } from '@angular/common/http';

if (environment.production) {
  enableProdMode();
}

export function initializeFactory(init: InicializadorService) {
  return () => init.inicializarApp();
}

bootstrapApplication(AppComponent, {
  providers: [
    SQLiteService,
    InicializadorService,
    ArticulosService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    provideIonicAngular(),
    provideRouter(routes),
    provideHttpClient(),
    {
      provide: APP_INITIALIZER,
      useFactory: initializeFactory,
      deps:[InicializadorService],
      multi: true
    }
  ],
});
